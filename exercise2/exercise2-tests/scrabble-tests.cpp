#include "gtest/gtest.h"
#include "scrabble.hpp"

struct ScrabbleTestSuite {};


TEST(ScrabbleTestSuite, Cabbage)
{
ASSERT_EQ(scrabble("CABBAGE"), 14);
}

TEST(ScrabbleTestSuite, Jenkins)
{
ASSERT_EQ(scrabble("JENKINS"), 18);
}

TEST(ScrabbleTestSuite, Quesadilla)
{
ASSERT_EQ(scrabble("QUESADILLA"), 20);
}

TEST(ScrabbleTestSuite, Programming)
{
ASSERT_EQ(scrabble("PROGRAMMING"), 19);
}