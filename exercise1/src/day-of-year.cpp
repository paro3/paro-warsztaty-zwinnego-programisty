#include "day-of-year.hpp"
#include <map>

int dayOfYear(int month, int dayOfMonth, int year) {
    std::map<int, int> monthDays = {
        {1, 31}, {2, 28}, {3, 31}, {4, 30}, {5, 31}, {6, 30},
        {7, 31}, {8, 31}, {9, 30}, {10, 31}, {11, 30}, {12, 31}
    };
    int dayOfYear{dayOfMonth};

    for(int m{1}; m < month; ++m)
        dayOfYear += monthDays[m];
    
    if (year%4 == 0 and month > 2)
        ++dayOfYear;

    return dayOfYear;
}

