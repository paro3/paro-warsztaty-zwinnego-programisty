#include "gtest/gtest.h"
#include "day-of-year.hpp"

struct DayOfYearTestSuite {};

TEST(DayOfYearTestSuite, January1stIsFitstDayOfYear)
{
  ASSERT_EQ(dayOfYear(1, 1, 2020), 1);
}

TEST(DayOfYearTestSuite, August2nd2016)
{
  ASSERT_EQ(dayOfYear(8, 2, 2016), 215);
}

TEST(DayOfYearTestSuite, April5th2003)
{
  ASSERT_EQ(dayOfYear(4, 5, 2003), 95);
}

TEST(DayOfYearTestSuite, December12th2005)
{
  ASSERT_EQ(dayOfYear(12, 12, 2005), 346);
}